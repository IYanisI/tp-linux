# I Partitionnement du serveur de stockage.

### 1. Partitionner le disque à l'aide de LVM.

#### (1) créer un physical volume (PV) :
#
#### La partie création.

>[yanis@storage ~] sudo pvcreate /dev/sdb   
  Physical volume "/dev/sdb" successfully created.    
>
>[yanis@storage ~]$ sudo pvcreate /dev/sdc   
  Physical volume "/dev/sdc" successfully created.
#
#### La partie vérification.
  
>[yanis@storage ~]$ sudo pvs 

 | PV | VG | Fmt | Attr | PSize | PFree |
 | -- | ---| --- | ---- | ------| ----- |  
 | /dev/sdb || lvm2 | --- | 2.00g | 2.00g|   
 | /dev/sdc || lvm2 | --- | 2.00g | 2.00g|

>[yanis@storage ~]$ sudo pvdisplay
"/dev/sdb" is a new physical volume of "2.00 GiB"   
  --- NEW Physical volume ---   
  PV Name      =         /dev/sdb    
  VG Name   
  PV Size     =           2.00 GiB    
  Allocatable  =         NO    
  PE Size       =        0   
  Total PE       =       0   
  Free PE         =      0   
  Allocated PE     =     0   
  PV UUID = VNkGPE-dCqX-rsT4-qzhL-nZZl-rXlz-yoCqsd    
>
 > "/dev/sdc" is a new physical volume of "2.00 GiB"    
  --- NEW Physical volume ---   
  PV Name          =     /dev/sdc    
  VG Name   
  PV Size          =     2.00 GiB    
  Allocatable      =     NO    
  PE Size          =     0   
  Total PE         =     0   
  Free PE          =     0   
  Allocated PE     =     0   
  PV UUID = 3zlaal-MKvl-R3sy-fxFg-wipf-lDCa-PbmW1p    

#
 #### (2) créer un nouveau volume group (VG) : 

#
#### La partie création.
>[yanis@storage ~]$ sudo vgcreate storage /dev/sdb    
  Volume group "storage" successfully created
>
>[yanis@storage ~]$ sudo vgextend storage /dev/sdc    
  Volume group "storage" successfully extended
#
#### La partie vérification. 

>[yanis@storage ~]$ sudo vgs

| VG | #PV | #LV | #SN | Attr | VSize | VFree |    
| -- | --- | --- | --- | ---- | ----- | ----- |       
|storage |  2 |  0 |  0 |wz--n-| 3.99g| 3.99g |   
>
>[yanis@storage ~]$ sudo vgdisplay    .
  --- Volume group ---      
  VG Name          =     storage     
  System ID     
  Format           =     lvm2    
  Metadata Areas   =     2   
  Metadata Sequence No = 2   
  VG Access        =     read/write    
  VG Status        =     resizable   
  MAX LV           =     0   
  Cur LV           =     0   
  Open LV          =     0     
  Max PV           =     0   
  Cur PV           =     2   
  Act PV           =     2   
  VG Size          =     3.99 GiB    
  PE Size          =     4.00 MiB    
  Total PE         =     1022    
  Alloc PE / Size = 0 / 0   
  Free  PE / Size = 1022 / 3.99 GiB   
  VG UUID = mdTwq9-n8Ed-Ez3O-BHHi-PuQ1-2uOJ-dFxX6j
  #
  #### (3) créer un nouveau logical volume (LV):
#
#### La partie création. 
>[yanis@storage ~]$ sudo lvcreate -l 100%FREE storage -n data   
Logical volume "data" created.
#
#### La partie vérification.

>[yanis@storage ~]$ sudo lvs

 | LV |  VG | Attr | LSize | Pool | Origin | Data% | Meta% | Move | Log | Cpy%Sync |      
 | -- | --- | ---- | ----- | ---- | ------ | ----- | ----- | ---- | --- | -------- |
|Convert|   
|data | storage | -wi-a----- | 3.99g   


>[yanis@storage ~]$ sudo lvdisplay    
  --- Logical volume ---    
  LV Path         =       /dev/storage/data   
  LV Name         =       data    
  VG Name         =       storage   
  LV UUID         =   sCsjne-KSvW-WG2D-OXfm-o76U-kI8z-qr6Ti7    
  LV Write Access    =    read/write   
  LV Creation host, time storage.tp4.linux, 2024-02-20 09:58:55 +0100   
  LV Status        =      available   
  #open          =       0    
  LV Size          =      3.99 GiB   
  Current LE      =       1022   
  Segments          =     2    
  Allocation         =    inherit    
  Read ahead sectors  =   auto   
  -currently set to   =  256    
  Block device         =  253:2    

### 2. Formater la partition.
>[yanis@storage ~]$ sudo mkfs -t ext4 /dev/storage/data   
mke2fs 1.46.5 (30-Dec-2021)     
Creating filesystem with 1046528 4k blocks and 261632 inodes    
Filesystem UUID: cf28ba44-24be-4994-b99a-f00cc97391ab   
Superblock backups stored on blocks:    
        32768, 98304, 163840, 229376, 294912, 819200, 884736    
>
>Allocating group tables: done   
Writing inode tables: done      
Creating journal (16384 blocks): done     
Writing superblocks and filesystem accounting information: done     

### 3. Monter la partition.

 #### (1) montage de la partition :

>[yanis@storage ~]$ sudo mount /dev/storage/data /mnt/storage/   
>
>[yanis@storage ~]$ df -h

| Filesystem | Size | Used | Avail | Use% | Mounted on |
| ---------- | ---- | ---- | ----- | ---- | ---------- |    
| devtmpfs | 4.0M | 0 | 4.0M |0% | /dev |
| tmpfs | 386M | 0 | 386M | 0% | /dev/shm |
| tmpfs | 155M | 3.7M | 151M | 3% | /run |
| /dev/mapper/rl-root | 6.2G | 1.2G | 5.0G | 20% / |
| /dev/sda1 | 1014M | 221M | 794M | 22% | /boot |
| tmpfs | 78M | 0 | 78M | 0% | /run/user/1000 |
| /dev/mapper/storage-data | 3.9G | 24K | 3.7G | 1% | /mnt/storage |

#### (2) utilisez un | grep pour isoler les lignes intéressantes :
[yanis@storage ~]$ mount |grep <span style="color:Red">storage</span> 
>/dev/mapper/<span style="color:Red">storage</span> -data on /mnt/<span style="color:Red">storage</span>  type ext4 (rw,relatime,seclabel)

#### (3) définir un montage automatique de la partition :

>[yanis@storage ~]$ cat /etc/fstab |grep <span style="color:Red">storage</span>    
/dev/<span style="color:Red">storage</span>   /data       /mnt/<span style="color:Red">storage</span>               ext4    defaults        0 0   
[yanis@storage ~] sudo umount /mnt/storage    
[yanis@storage ~] sudo mount -av    
/                        : ignored    
/boot                    : already mounted    
none                     : ignored    
mount: /mnt/storage does not contain SELinux labels.    
       You just mounted a file system that supports labels which does not   
       contain labels, onto an SELinux box. It is likely that confined    
       applications will generate AVC messages and not be allowed access to   
       this file system.  For more details see restorecon(8) and mount(8).    
mount: (hint) your fstab has been modified, but systemd still uses
       the old version; use 'systemctl daemon-reload' to reload.    
/mnt/storage             : successfully mounted   