# I Service SSH.

## (1) Analyse du service.
### 1. S'assurer que le service sshd est démarré.

>[yanis@node1 ~]$ systemctl status      
<span style="color:Green">●</span> node1.tp2.b1      
    State: <span style="color:Green">running</span>        
    Units: 283 loaded (incl. loaded aliases)        
     Jobs: 0 queued     
   Failed: 0 units      
    Since: Mon 2024-01-29 11:23:40 CET; 11min ago       
  systemd: 252-13.el9_2     
   CGroup: /        

### 2. Analyser les processus liés au service SSH

>[yanis@node1 ~]$ ps -ef | grep sshd        
root         683       1  0 11:23 ?        00:00:00 <span style="color:Red">sshd</span>: /usr/sbin/<span style="color:Red">sshd</span> -D [listener] 0 of 10-100 startups     
root        1313     683  0 11:24 ?        00:00:00 <span style="color:Red">sshd</span>: yanis [priv]      
yanis       1319    1313  0 11:25 ?        00:00:00 <span style="color:Red">sshd</span>: yanis@pts/0       
yanis       1378    1320  0 11:47 pts/0    00:00:00 grep --color=auto <span style="color:Red">sshd</span>    

### 3. Déterminer le port sur lequel écoute le service SSH.

>[yanis@node1 ~]$ ss -a | grep ssh      
tcp   LISTEN 0      128                      0.0.0.0:ssh                     0.0.0.0:*          
tcp   ESTAB  0      52                                      10.2.1.11:ssh                    10.2.1.1:49865     
tcp   LISTEN 0      128                                          [::]:ssh  [::]:*

### 4 /1. Consulter les logs du service SSH.
>[yanis@node1 ~]$ journalctl -xe -u sshd     
~       
Jan 29 11:23:49 node1.tp2.b1 systemd[1]: Starting OpenSSH server daemon...      
<span style="color:Green">Subject: A start job for unit sshd.service has begun execution       
Defined-By: systemd      
Support: https://access.redhat.com/support       
A start job for unit sshd.service has begun execution.          
>
>The job identifier is 222.</span>      
Jan 29 11:23:49 node1.tp2.b1 sshd[683]: main: sshd: ssh-rsa algorithm is disabled       
Jan 29 11:23:49 node1.tp2.b1 sshd[683]: Server listening on 0.0.0.0 port 22.        
Jan 29 11:23:49 node1.tp2.b1 sshd[683]: Server listening on :: port 22.     
Jan 29 11:23:49 node1.tp2.b1 systemd[1]: Started OpenSSH server daemon.     
<span style="color:Green">Subject: A start job for unit sshd.service has finished successfully     
Defined-By: systemd              
Support: https://access.redhat.com/support  </span>      
>
><span style="color:Green">A start job for unit sshd.service has finished successfully.</span>     
>
><span style="color:Green">The job identifier is 222. </span>     
Jan 29 11:24:57 node1.tp2.b1 sshd[1313]: main: sshd: ssh-rsa algorithm is disabled      
Jan 29 11:25:32 node1.tp2.b1 sshd[1313]: Accepted password for yanis from 10.2.1.1 port 49865 ssh2      
Jan 29 11:25:32 node1.tp2.b1 sshd[1313]: pam_unix(sshd:session): session opened for user yanis(uid=1000) by (uid=0)     

### 4 /2. Consulter les logs du service SSH.

>[yanis@node1 log]$ sudo tail -n 10 /var/log/secure             
Jan 30 09:57:59 node1 sudo[1732]: pam_unix(sudo:session): session closed for user       
Jan 30 09:58:05 node1 sudo[1735]:   yanis : TTY=pts/1 ; PWD=/var/log ; USER=root ; COMMAND=/bin/tail -n 10 /var/log/secure      
Jan 30 09:58:05 node1 sudo[1735]: pam_unix(sudo:session): session opened for user root(uid=0) by yanis(uid=1000)        
Jan 30 09:58:05 node1 sudo[1735]: pam_unix(sudo:session): session closed for user root      
Jan 30 09:58:11 node1 sudo[1739]:   yanis : TTY=pts/1 ; PWD=/var/log ; USER=root ; COMMAND=/bin/tail -10 /var/log/secure        
Jan 30 09:58:11 node1 sudo[1739]: pam_unix(sudo:session): se ssion opened for user root(uid=0) by yanis(uid=1000)       
Jan 30 09:58:11 node1 sudo[1739]: pam_unix(sudo:session): session closed for user root      
Jan 30 09:58:15 node1 sudo[1742]:   yanis : TTY=pts/1 ; PWD=/var/log ; USER=root ; COMMAND=/bin/tail -n 10 /var/log/secure      
Jan 30 09:58:15 node1 sudo[1742]: pam_unix(sudo:session): session opened for user root(uid=0) by yanis(uid=1000)        
Jan 30 09:58:15 node1 sudo[1742]: pam_unix(sudo:session): session closed for user root      

## (2) Modification du service.

### 1. Identifier le fichier de configuration du serveur SSH.
>[yanis@node1 //]$ ls -a /etc/ssh/       
.   moduli      ssh_config.d  sshd_config.d       ssh_host_ecdsa_key.pub  ssh_host_ed25519_key.pub  ssh_host_rsa_key.pub        
..  ssh_config  sshd_config   ssh_host_ecdsa_key  ssh_host_ed25519_key    ssh_host_rsa_key

#### Je pense qu'il s'agit du fichier sshd_config.

##

### 2. Modifier le fichier de conf.
>[yanis@node1 //] echo $RANDOM
10307       
[yanis@node1 //] sudo cat /etc/ssh/sshd_config |grep Port      
<span style="color:Red">Port</span> 10307     
#Gateway<span style="color:Red">Port</span>s no                   
[yanis@node1 //] sudo firewall-cmd --remove-port=22/tcp --permanent                
<span style="color:Red">Warning: NOT_ENABLED: 22:tcp</span>    
success
[yanis@node1 //] sudo firewall-cmd --add-port=10307/tcp --permanent     
success     
[yanis@node1 //] sudo firewall-cmd --reload     
success     
[yanis@node1 //] sudo firewall-cmd --list-all |grep ports       
<span style="color:Red">ports</span>: 10307/tcp      
forward-<span style="color:Red">ports</span>:                
source-<span style="color:Red">ports</span>:      

### 3. Redémarrer le service.
>[yanis@node1 //]$ sudo systemctl restart sshd

###  4. Effectuer une connexion SSH sur le nouveau port.
>PS C:\Users\yanis> ssh yanis -p 10307       
yanis@yanis's password:     
Last login: Tue Jan 30 11:56:10 2024 from 10.2.1.1   