# III. Your own services

## 2. Analyse des services existants

### 1. Afficher le fichier de service SSH.

>[yanis@node1 ~]$ systemctl status sshd         
● sshd.service - OpenSSH server daemon          
     Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; preset: enabled)            
     Active: active (running) since Sun 2024-02-04 23:14:57 CET; 11min ago          
       Docs: man:sshd(8)            
             man:sshd_config(5)         
   Main PID: 683 (sshd)         
      Tasks: 1 (limit: 4672)            
     Memory: 6.3M       
        CPU: 89ms           
     CGroup: /system.slice/sshd.service             
             └─683 "sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups"            
>
>Feb 04 23:14:57 node1.tp2.b1 sshd[683]: Server listening on 0.0.0.0 port 22.            
Feb 04 23:14:57 node1.tp2.b1 sshd[683]: Server listening on :: port 22.         
Feb 04 23:14:57 node1.tp2.b1 systemd[1]: Started OpenSSH server daemon.             
Feb 04 23:15:00 node1.tp2.b1 sshd[1277]: main: sshd: ssh-rsa algorithm is disabled              
Feb 04 23:15:03 node1.tp2.b1 sshd[1277]: Accepted password for yanis from 10.2.1.1 port 52449 ssh2              
Feb 04 23:15:03 node1.tp2.b1 sshd[1277]: pam_unix(sshd:session): session opened for user yanis(uid=1000) by (uid=0)                 
Feb 04 23:15:03 node1.tp2.b1 sshd[1277]: pam_unix(sshd:session): session closed for user yanis           
Feb 04 23:15:13 node1.tp2.b1 sshd[1305]: main: sshd: ssh-rsa algorithm is disabled                  
Feb 04 23:15:15 node1.tp2.b1 sshd[1305]: Accepted password for yanis from 10.2.1.1 port 52450 ssh2              
Feb 04 23:15:15 node1.tp2.b1 sshd[1305]: pam_unix(sshd:session): session opened for user yanis(uid=1000) by (uid=0)             


>[yanis@node1 //]$ cat lib/systemd/system/sshd.service | grep ExecStart=        
<span style="color:Red">ExecStart=</span>/usr/sbin/sshd -D $OPTIONS        

### 2. Afficher le fichier de service NGINX.

>[yanis@node1 //]$ cat /lib/systemd/system/nginx.service | grep ExecStart=     
<span style="color:Red">ExecStart=</span>/usr/sbin/nginx

## 3. Création de service.

### 1. Créez le fichier /etc/systemd/system/tp3_nc.service.
>[yanis@node1 //] echo $RANDOM
10741
[yanis@node1 //] sudo nano /etc/systemd/system/tp3_nc.service        
[yanis@node1 //] sudo cat /etc/systemd/system/tp3_nc.service      
[Unit]           
Description=Super netcat tout fou            
>
>[Service]      
ExecStart=/usr/bin/nc -l 10741 -k      

### 2. Indiquer au système qu'on a modifié les fichiers de service.
>[yanis@node1 //]$ sudo systemctl daemon-reload

### 3. Démarrer notre service de ouf.

>[yanis@node1 //]$ systemctl start tp3_nc.service

### 4. Vérifier que ça fonctionne.
>[yanis@node1 //]$ systemctl status tp3_nc.service     
>● tp3_nc.service - Super netcat tout fou     
     Loaded: loaded (/etc/systemd/system/tp3_nc.service;static)     
     Active: active (running) since Sun 2024-02-04 23:56:10 CET; 57s ago      
   Main PID: 1494 (nc)              
      Tasks: 1 (limit: 4672)        
     Memory: 780.0K        
        CPU: 3ms        
     CGroup: /system.slice/tp3_nc.service       
             └─1494 /usr/bin/nc -l 10741 -k        
>
>Feb 04 23:56:10 node1.tp2.b1 systemd[1]: Started Super netcat tout fou.
       