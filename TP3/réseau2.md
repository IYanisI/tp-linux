# II Service HTTP.

## (1) Mise en place.
### 1. Installer le serveur NGINX.
>[yanis@node1 ~]$ dnf search NGINX       
>Last metadata expiration check: 0:18:42 ago on Tue 30 Jan 2024 11:59:20 AM CET.        
======================= Name & Summary Matched: NGINX =======================         
<span style="color:Purple">nginx</span>-all-modules.noarch : A meta package that installs all available <span style="color:Purple">Nginx</span>  modules         
<span style="color:Purple">nginx</span>-core.x86_64 : <span style="color:Purple">nginx</span> minimal core          
<span style="color:Purple">nginx</span>-filesystem.noarch : The basic directory layout for the <span style="color:Purple">Nginx</span> server                 
<span style="color:Purple">nginx</span>-mod-http-image-filter.x86_64 : <span style="color:Purple">Nginx</span>  HTTP image filter module                    
<span style="color:Purple">nginx</span>-mod-http-perl.x86_64 : <span style="color:Purple">Nginx</span>  HTTP perl module          
<span style="color:Purple">nginx</span>-mod-http-xslt-filter.x86_64 : <span style="color:Purple">Nginx</span>  XSLT module           
<span style="color:Purple">nginx</span>-mod-mail.x86_64 : <span style="color:Purple">Nginx</span>  mail modules              
<span style="color:Purple">nginx</span>-mod-stream.x86_64 : <span style="color:Purple">Nginx</span> stream modules              
pcp-pmda-<span style="color:Purple">nginx</span>.x86_64 : Performance Co-Pilot (PCP) metrics for the <span style="color:Purple">Nginx</span>  Webserver              
======================= Name Matched: NGINX =======================               
<span style="color:Purple">nginx</span>.x86_64 : A high performance web server and reverse proxy server       

### 2. Démarrer le service NGINX.
>[yanis@node1 ~]$ sudo systemctl start nginx         
[yanis@node1 ~]$ sudo systemctl status nginx        
● nginx.service - The nginx HTTP and reverse proxy server           
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; preset: disabled)         
     Active: active (running) since Sun 2024-02-04 19:12:03 CET; 2s ago             
    Process: 1453 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)            
    Process: 1454 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)           
    Process: 1455 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)         
   Main PID: 1456 (nginx)           
      Tasks: 2 (limit: 4672)            
     Memory: 1.9M       
        CPU: 19ms       
     CGroup: /system.slice/nginx.service        
             ├─1456 "nginx: master process /usr/sbin/nginx"         
             └─1457 "nginx: worker process"         
>
>Feb 04 19:12:03 node1.tp2.b1 systemd[1]: Starting The nginx HTTP and reverse proxy server...        
Feb 04 19:12:03 node1.tp2.b1 nginx[1454]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok      
Feb 04 19:12:03 node1.tp2.b1 nginx[1454]: nginx: configuration file /etc/nginx/nginx.conf test is successful        
Feb 04 19:12:03 node1.tp2.b1 systemd[1]: Started The nginx HTTP and reverse proxy server.       

### 3. Déterminer sur quel port tourne NGINX.
#### 1/ Vous devez filtrer la sortie de la commande utilisée pour n'afficher que les lignes demandées.
>[yanis@node1 ~]$ sudo cat /etc/nginx/nginx.conf |grep listen      
        listen       80;         
        listen       [::]:80;    
>#listen       443 ssl http2;      
>#listen       [::]:443 ssl http2;       

#### Nginx écoute sur le port 80.
##
#### 2/ Ouvrez le port concerné dans le firewall
>[yanis@node1 ~] sudo firewall-cmd --add-port=80/tcp --permanent     
success     
>[yanis@node1 ~] sudo firewall-cmd --reload     
success     
## 
### 4. Déterminer les processus liés au service NGINX.
[yanis@node1 ~]$ ps aux | grep nginx         
root        1479  0.0  0.1  10108   956 ?        Ss   19:22   0:00 <span style="color:Red">nginx</span>: master process /usr/sbin/<span style="color:Red">nginx</span>        
<span style="color:Red">nginx</span>       1480  0.0  0.6  13908  5016 ?        S    19:22   0:00 <span style="color:Red">nginx</span>: worker process             
yanis       1625  0.0  0.2   6408  2304 pts/0    S+   19:50   0:00 grep --color=auto <span style="color:Red">nginx</span>             

### 5. Déterminer le nom de l'utilisateur qui lance NGINX.
[yanis@node1 ~]$ cat /etc/passwd | grep yanis         
<span style="color:Red">yanis</span> :x:1000:1000:<span style="color:Red">yanis</span> :/home/<span style="color:Red">yanis</span> :/bin/bash

### 5. Test !

#### visitez le site web.
http://10.2.1.11:80

>[yanis@node1 ~]$ curl -SLO 10.2.1.11/80   

| % Total |   % Received |% Xferd | Average| Speed |  Time |   Time  |   Time  |Current |    
| ------- | ------------ | ------ | ------ | ----- | ----- | ------- | ------- | ------ |
|         |              |        | Dload  | Upload| Total |  Spent  |   Left  |  Speed |   
|100 3332 |100    3332   | 0    0 | 1318k   |      0|--:--:--| --:--:--| --:--:--|  1626k |    

><.!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
>
><.html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">     
  <.head>      
    <.title>The page is not found<./title>
    <.meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />    
    <.style type="text/css">     
      <![CDATA[      
      body {      
      >  background-color: #fff;

## (2) Analyser la conf de NGINX.


### 1. Déterminer le path du fichier de configuration de NGINX.
>[yanis@node1 ~]$ ls -al /etc/nginx/nginx.conf     
-rw-r--r--. 1 root root 2334 Oct 16 20:00 /etc/nginx/nginx.conf

### 2. Trouver dans le fichier de conf.
>[yanis@node1 ~]$ cat /etc/nginx/nginx.conf | grep server -A 16             
    <span style="color:Red">serveur</span> {            
        listen       80;            
        listen       [::]:80;          
        <span style="color:Red">serveur</span>_name  _;          
        root         /usr/share/nginx/html;           
>        # Load configuration files for the default <span style="color:Red">serveur</span> block.           
        include /etc/nginx/default.d/*.conf;          
>        error_page 404 /404.html;         
        location = /404.html {         
        }            
>        error_page 500 502 503 504 /50x.html;         
>        location = /50x.html {               
>        }         
>    }    

>[yanis@node1 ~]$ cat /etc/nginx/nginx.conf | grep include        
<span style="color:Red">include</span> /usr/share/nginx/modules/.conf;        
    <span style="color:Red">include</span>/etc/nginx/mime.types;        
    # See http://nginx.org/en/docs/ngx_core_module.html#<span style="color:Red">include</span>         
    <span style="color:Red">include</span> /etc/nginx/conf.d/.conf;        
        <span style="color:Red">include</span> /etc/nginx/default.d/.conf;       
#<span style="color:Red">include</span> /etc/nginx/default.d/.conf; 


## (3) Déployer un nouveau site web.

### 1. Créer un site web.
>[yanis@node1 tp3_linux] cd //     
[yanis@node1 //] sudo mkdir -p var/www/tp3_linux   
[yanis@node1 //] sudo nano var/www/tp3_linux/index.html
[yanis@node1 //] cat var/www/tp3_linux/index.html     
><1> MEOW mon premier serveur web </1>

### 2. Gérer les permissions.
>[yanis@node1 //]$ sudo chmod 700 var/www/tp3_linux

### 3. Adapter la conf NGINX.

#### 1/ Dans le fichier de conf principal.

>[yanis@node1 ~]$ sudo nginx -s reload

#### 2/ Créez un nouveau fichier de conf.

>[yanis@node1 //]$ echo $RANDOM
27855
[yanis@node1 //]$ sudo nano /etc/nginx/conf.d/ser.conf
[yanis@node1 //]$ sudo cat /etc/nginx/conf.d/ser.conf
server {
>
>  listen 27855;
>
>  root /var/www/tp3_linux;
}

### 4. Visitez votre super site web.
$  curl 10.2.1.11 |head    
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current     
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:--  0:00:02 --:--:--     0            
curl: (7) Failed to connect to 10.2.1.11 port 80 after 2011 ms: Couldn't connect to server      
