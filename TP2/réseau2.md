# I. Programmes et processus.

## (1) Run then kill.

### 1. Lancer un processus sleep.

#### Terminal 1.
> [yanis@localhost ~]$ sleep 1000           
>|
#### Terminal 2.

>[yanis@localhost ~]$ ps -u | grep sleep        
yanis       1621  0.0  0.1   5584  1016 pts/1    S+   09:56   0:00 <span style="color:red">sleep</span> 1000       
yanis       1630  0.0  0.2   6408  2296 pts/2    S+   09:57   0:00 grep --color=auto <span style="color:red">sleep</span>    
#
### 2. Terminez le processus sleep depuis le deuxième terminal

>[yanis@localhost ~]$ kill 1621

## (2) Tâche de fond.

### 1. Lancer un nouveau processus sleep, mais en tâche de fond.

>[yanis@localhost ~]$ sleep 1000 &      
[1] 1669

### 2. Visualisez la commande en tâche de fond.

>[yanis@localhost ~]$ ps -u

| USER | PID | %CPU | %MEM | VSZ | RSS | TTY | STAT | START | TIME | COMMAND |      
| ---- | --- | ---- | ---- | --- | --- | --- | ---- | ----- | ---- | ------- |
yanis | 1293 | 0.0 | 0.5 |  7440 | 4224 |tty1  |   Ss+ | Jan22|   0:00| -bash|        
yanis     |  1427 | 0.0 | 0.5 |  7440 | 4320 |pts/0  |  Ss |  Jan22 |  0:00| -bash |       
yanis|       1549 | 0.0 | 0.5  | 7616 | 4412 |pts/1 |   Ss  | 09:13  | 0:00| -bash |       
yanis |      1578  |0.0  |0.5|   7440  |4320| pts/2   | Ss+|  09:15  | 0:00 |-bash        
yanis  |     1669 | 0.0  |0.1 |  5584 | 1016 |pts/1  |  S   | 10:06  | 0:00 |sleep 100        
yanis   |    1676  |0.0 | 0.4  |10140|  3588 |pts/1 |   R+   |10:09 |  0:00 |ps -u 

#### Le chiffre 1669 est le PID de sleep. 

## (3) Find paths.
### 1. Trouver le chemin où est stocké le programme sleep.
>[root@localhost //]# ls -al /usr/bin/sleep | grep sleep

### 2. trouver les chemins vers tous les fichiers qui s'appellent .bashrc. 

>[root@localhost //]# find   /   -name .bashrc      
/etc/skel/.bashrc       
/root/.bashrc       
/home/yanis/.bashrc     
/home/papier_alu/.bashrc        

## (4) La variable PATH.
### Sleep
>[root@localhost //]# which sleep       
/usr/bin/sleep   
#   
### Bin
>[root@localhost //]# which bin     
/usr/bin/which: no bin in (/root/.local/bin:/root/bin:/home/yanis/.local/bin:/home/yanis/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin)
#
### Ping
>[root@localhost //]# which ping        
/usr/bin/ping           
#

# II. Paquets.

### 1. Installer le paquet firefox.
>[root@localhost git]# dnf install git

### 2. Utiliser une commande pour lancer Git.

>[root@localhost yanis]# find / -name git        
/usr/bin/git

#### Pour l'executer :
>[yanis@localhost ~]$ git

### 3. Installer le paquet nginx.
>[root@localhost yanis]# dnf install nginx

### 4. Déterminer.

#### Chemin vers le dossier de logs.
>[root@localhost yanis]# ls -al /var/log/nginx | grep nginx
#
#### Chemin vers le dossier de configuration.
>[root@localhost yanis]# ls -al etc/nginx/nginx.conf | grep nginx
#
### 5. Mais aussi déterminer.