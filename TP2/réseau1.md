# I Fichier.

## (1) Find me
### 1. Trouver le chemin vers le répertoire personnel de votre utilisateur.

>[yanis@localhost ~] ls -a       
>[yanis@localhost ~] cd //      
>[yanis@localhost //] ls -a        
.   afs  boot  etc   lib    media  opt   root  sbin  sys  usr           
..  bin  dev   home  lib64  mnt    proc  run   srv   tmp  var       
[yanis@localhost //] cd home              
[yanis@localhost home] ls       
yanis   
[yanis@localhost home] cd yanis     
[yanis@localhost yanis] ls -a       
.  ..  .bash_history  .bash_logout  .bash_profile  .bashrc  .ssh        

### 2. Trouver le chemin du fichier de logs SSH.

>[yanis@localhost //] cd /var/log/      
[yanis@localhost log] sudo cat secure       

#### ou

>journalctl -u sshd  


### 3. Trouver le chemin du fichier de configuration du serveur SSH

>[yanis@localhost //] cd /etc/ssh/       
[yanis@localhost ssh] ls        
moduli      ssh_config.d  sshd_config.d       ssh_host_ecdsa_key.pub  ssh_host_ed25519_key.pub  ssh_host_rsa_key.pub        
ssh_config  sshd_config   ssh_host_ecdsa_key  ssh_host_ed25519_key    ssh_host_rsa_key              
[yanis@localhost ssh] cat ssh_config        

## II Users.

## (1) Nouveau user.

>[yanis@localhost ssh] sudo useradd marmotte -m -d /home/papier_alu/
>[yanis@localhost ssh] cd //        
[yanis@localhost //] sudo passwd marmotte       

## (2) Infos enregistrées par le système.

### 1. Prouver que cet utilisateur a été créé et déterminer le hash du password de l'utilisateur marmotte

>[yanis@localhost //]$ sudo cat /etc/shadow | grep marmotte      
<span style="color:red">marmotte</span>:$6$2LJoG2zqpzuukug2$xKgavW1t98YWP2CbazBXvTxIjO82env7L3Ihw.L01FSm1HwNH55v3eyPnhDIYAVyX.CLKMBSEE7H9nGwdIY6P/:19744:0:99999:7:::

## III Connexion sur le nouvel utilisateur.

### 1. fermer la session de l'utilisateur.

>[yanis@localhost home]$ exit

#### ou

>[yanis@localhost home]$  sudo skill -KILL -u yanis

### 2. Assurez-vous que vous pouvez vous connecter en tant que l'utilisateur marmotte.

>PS C:\Users\yanis> ssh 10.4.1.2        
>yanis@ 10.4.1.2's password:      
Last login: Mon Jan 22 12:18:29 2024 from 10.4.1.1      
[yanis@localhost ~] su - marmotte      
Password:       
Last login: Mon Jan 22 12:19:31 CET 2024 on pts/0              
[marmotte@localhost ~] ls /home/yanis/      
ls: cannot open directory '/home/yanis/': Permission denied     