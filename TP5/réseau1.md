# I Script carte d'identité.

### [Script](idcard.sm)

>[yanis@code ~] sudo /srv/idcard/idcard.sh                 
[yanis@code ~]$ /srv/idcard/idcard.sh       
Machine name : code.tp5.linux       
OS : ID="rocky" and kernel version is VERSION_ID="9.2"      
IP : 10.5.1.8/24        
10.0.3.15/24        
Disk : 5240088      
Top 5 processes by RAM usage :      
python3 -s      
NetworkManager --no-daemon      
systemd     
systemd     
systemd     
Listening ports :       
 68 udp : udp ESTAB 0 0 10.0.3.15%enp0s8:68 10.0.3.2:67         
 323 udp : udp UNCONN 0 0 127.0.0.1:323 0.0.0.0:*       
 22 tcp : tcp LISTEN 0 128 0.0.0.0:22 0.0.0.0:*         
 22 tcp : tcp ESTAB 0 0 10.5.1.8:22 10.5.1.1:53853       
PATH directories :      
/home/yanis/.local/bin      
/home/yanis/bin     
/usr/local/bin      
/usr/bin        
/usr/local/sbin     
https://cdn2.thecatapi.com/images/q9.jpg            