# II Script youtube-dl

## 1. Premier script youtube-dl

### [[Script]](yt.sh)

### [[Log]](download.log)

>[yanis@code /]$ /srv/yt/yt.sh https://www.youtube.com/watch?v=zU6PGOAlVqI      
Video https://www.youtube.com/watch?v=zU6PGOAlVqI was downloaded.       
path : /srv/yt/downloads/Lyin_(See_You_in_the_Next_World)/Lyin_(See_You_in_the_Next_World).mp4

## 2. MAKE IT A SERVICE

### [[Script]](yt-v2.sh)

### [[Service]](download.log)

>[yanis@code downloads]$ systemctl status yt    
● yt.service - Ceci est une application qui permet le téléchargement de vidéo youtube. Rien que ça c'est tout ce qu'il fait. Au moins il n'y aura pas de pu>      
     Loaded: loaded (/etc/systemd/system/yt.service; disabled; preset: disabled)    
     Active: active (running) since Thu 2024-03-07 10:40:17 CET; 1min 8s ago     
   Main PID: 1473 (yt-v2.sh)     
      Tasks: 2 (limit: 4672)     
     Memory: 17.5M      
        CPU: 18.873s    
     CGroup: /system.slice/yt.service     
             ├─1473 /bin/bash /srv/yt/yt-v2.sh     
             └─1499 sleep 30     



>[yanis@code downloads]$ journalctl -xe -u yt      
~     
~     
Mar 07 10:40:17 code.tp5.linux systemd[1]: Started Ceci est une application qui permet le téléchargement de vidéo youtube. Rien que ça c'est tout ce qu'il >      
Subject: A start job for unit yt.service has finished successfully      
Defined-By: systemd     
Support: https://access.redhat.com/support      
>
>A start job for unit yt.service has finished successfully.     
>
>The job identifier is 838.    
Mar 07 10:40:17 code.tp5.linux yt-v2.sh[1473]: Cette ligne n'es pas une URL      
Mar 07 10:41:19 code.tp5.linux yt-v2.sh[1473]: Video https://www.youtube.com/watch?v=gVHD6ieRmTI was downloaded.     
Mar 07 10:41:19 code.tp5.linux yt-v2.sh[1473]: path : /srv/yt/downloads/Animated_Short_[Meteoric_Salvation]_Japanese_Dub_Version_-_Honkai_Impact_3rd/Animat>      
lines 1-11/11 (END)     