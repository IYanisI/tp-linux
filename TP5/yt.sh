#!/bin/bash
#yanis
#04/03/2024

i=$(youtube-dl --skip-download --get-title --no-warnings $@)
i=${i// /_}
mkdir /srv/yt/downloads/$i/ 1> /dev/null 2> /dev/null
youtube-dl -o /srv/yt/downloads/$i/d --write-description --skip-download $@ 1> /dev/null 2> /dev/null
mv /srv/yt/downloads/$i/d.description /srv/yt/downloads/$i/description 1> /dev/null 2> /dev/null
youtube-dl -o /srv/yt/downloads/$i/$i.mp4 $@ 1> /dev/null 2> /dev/null
echo "Video $@ was downloaded."
echo "path : /srv/yt/downloads/$i/$i.mp4"
FILE=/var/log/yt/download.log
if [ -f "$FILE" ]; then
echo "[$(date "+%D %T")] Video $@ was downloaded. File path : /srv/yt/downloads/$i/$i.mp4" >> $FILE
    else
exit
fi
