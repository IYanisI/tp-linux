#!/bin/bash
#yanis
#05/03/2024

Path=/srv/yt/DownFile.txt
REGEXKEY="^https:\/\/www\.youtube\.com\/watch\?v=([0-9a-zA-Z]){11}$"
touch $Path
while :
do
while IFS= read -r line
do
if [[ $line =~ $REGEXKEY ]]
then
i=$(youtube-dl --skip-download --get-title --no-warnings $line)
i=${i// /_}
mkdir /srv/yt/downloads/$i/ 1> /dev/null 2> /dev/null
youtube-dl -o /srv/yt/downloads/$i/d --write-description --skip-download $line 1> /dev/null 2> /dev/null
mv /srv/yt/downloads/$i/d.description /srv/yt/downloads/$i/description 1> /dev/null 2> /dev/null
youtube-dl -o /srv/yt/downloads/$i/$i.mp4 $line 1> /dev/null 2> /dev/null
echo "Video $line was downloaded."
echo "path : /srv/yt/downloads/$i/$i.mp4"
FILE=/var/log/yt/download.log
if [ -f "$FILE" ]; then
echo "[$(date "+%D %T")] Video $line was downloaded. File path : /srv/yt/downloads/$i/$i.mp4" >> $FILE
else
exit
fi
sed -i '1d' $Path
sleep 30
else
	echo "Cette ligne n'es pas une URL"
	sed -i '1d' $Path
sleep 30
fi
done < $Path
done
