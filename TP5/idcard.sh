#!/bin/bash
#yanis
#23/02/2024

echo "Machine name : $(hostnamectl | grep hostname | cut -d' ' -f4)"
echo "OS : $(cat /etc/os-release | grep ID | sed -n '1p') and kernel version is $(cat /etc/os-release | grep ID | sed -n '3p')"
echo "IP : $(ip a | grep global | cut -d' ' -f6)"
echo "Disk : $(df | grep root | cut -d' ' -f8)"
echo "Top 5 processes by RAM usage :"
echo "$(ps -eo command --sort=-%mem | head -6 | tail -5 | cut -d'/' -f4)"

echo "Listening ports :"
while read coli ; do

  port="$(echo $coli | cut -d':' -f2 | cut -d' ' -f1)"
  tup="$(echo $coli | cut -d' ' -f1)"
  ssh="$(echo $coli | cut -d'"' -f2)"

echo " ${port} ${tup} : ${ssh} "
done <<< "$(ss -u -lnpat4 -H)"

i=1
toto=${PATH//[^:]}
toto=${#toto}
echo "PATH directories :"
while [[ $i -le $toto ]]
do
	m=$(echo $PATH | cut -d':' -f$i)
	echo "$m"
	((i++))	
done
cat_url="$(curl -s https://api.thecatapi.com/v1/images/search | cut -d'"' -f8)"
echo "${cat_url}"
